.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


=====================
Nominatim Geolocalize
=====================

Get partner coordinate using Nominatim API

Alternative to Google API



Credits
=======

Contributors ------------

* Benjamin Rivier <benjamin@le-filament.com>



Maintainer ----------

.. image:: https://le-filament.com/images/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament
