# -*- coding: utf-8 -*-

# © 2017 Le Filament (<http://www.le-filament.com>)
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).


{
    'name': 'Nominatim Geolocalize',
    'summary': 'Get partner coordinate using Nominatim API',
    'version': '10.0.1.0.0',
    'license': 'LGPL-3',
    'category': 'Partner',
    'author': 'Odoo SA, Le Filament',
    'description': """
Nominatim Geolocalize
========================

Get partner coordinate using Nominatim API

    """,
    'depends': ['base'],
    'data': [
        'datas/cron.xml',
        'views/res_partner.xml',
    ],
    'installable': True,
}
